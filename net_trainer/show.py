#!/usr/bin/env python3

import sys

import numpy as np

import matplotlib.pyplot as plt

lines = list(tuple(float(e.strip()) for e in ln.split(',')) for ln in sys.stdin)

x = np.fromiter((ln[0] for ln in lines), np.float32)
y = np.fromiter((ln[1] for ln in lines), np.float32)
z = np.fromiter((ln[2] for ln in lines), np.float32)

c = np.arange(x.shape[0])


plt.plot(c, x, c, y, c, z)
plt.show()
