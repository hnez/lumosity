#!/usr/bin/env python3

import serial

ser = serial.Serial('/dev/ttyUSB0', 115_200)
ser.readline()

while True:
    line = ser.readline().decode('ascii').strip()

    hex_data = line.split(' ')

    acc_shorts = list(int(h, 0) for h in hex_data)

    acc_signed = tuple(
        acc if (acc & 0x8000) == 0 else -((acc ^ 0xffff) + 1)
        for acc in acc_shorts
    )

    acc_scaled = tuple(
        acc / 2**15
        for acc in acc_signed
    )

    print(', '.join(map(str, acc_scaled)))
