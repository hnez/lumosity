import os

import torch

from torch.utils.data import Dataset

class SplitDataset(Dataset):
    def __init__(self, base, start, end):
        self.base = base
        self.start = start
        self.end = end

    def __len__(self):
        return (self.end - self.start)

    def __getitem__(self, idx):
        if idx >= len(self):
            raise IndexError('dataset index out of range')

        return self.base[self.start + idx]

    @classmethod
    def split(cls, dataset, ratio, minimum=0):
        full_len = len(dataset)
        first_len = int(full_len * ratio)

        if first_len < minimum:
            first_len = minimum

        if (full_len - first_len) < minimum:
            first_len = full_len - minimum

        if first_len < minimum:
            raise Exception('SplitDataset could not fulfill minimum of {}'.format(minimum))

        first = cls(dataset, 0, first_len)
        second = cls(dataset, first_len, full_len)

        return (first, second)


class LumosityDataset(Dataset):
    def __init__(self, base_dir, categories):
        self.names = list()
        self.cache = dict()
        self.categories = categories

        for cat in self.categories:
            cat_dir = os.path.join(base_dir, cat)

            cat_names = list()

            for (root, dirs, files) in os.walk(cat_dir):
                for fname in files:
                    fqname = os.path.join(root, fname)

                    if not fqname.endswith('.csv'):
                        continue

                    cat_names.append(fqname)

            self.names.append(cat_names)

    def _read_ds(self, fname):
        if fname in self.cache:
            return self.cache[fname]

        data_x = list()
        data_y = list()
        data_z = list()

        with open(fname, 'r') as fd:
            for i, ln in enumerate(fd):
                x, y, z = tuple(float(a.strip()) for a in ln.split(','))

                data_x.append(x)
                data_y.append(y)
                data_z.append(z)

        n_samples = len(data_x)

        inputs = (torch.Tensor([data_x, data_y, data_z]) * 4096.0).floor()

        self.cache[fname] = inputs

        return inputs

    def __len__(self):
        return min(len(n) for n in self.names)

    def __getitem__(self, idx):
        sequences = tuple(
            self._read_ds(name[idx])
            for name in self.names
        )

        sequences = torch.cat(sequences, 1)

        labels = torch.zeros(len(self.categories), sequences.shape[1]//8)

        for cat_idx, cat_name in enumerate(self.categories):
            pidx = int((160 + 450 * cat_idx) / 8 + 0.5)

            labels[cat_idx,pidx-1:pidx+2] = 3500

        return (sequences, labels)
