#!/usr/bin/env python3

import torch
import torch.nn as nn

class LumosityNet(nn.Module):
    def __init__(self, base=None):
        super().__init__()

        self.conv1 = nn.Conv1d(3, 8, 9, 2, 4)
        self.conv2 = nn.Conv1d(8, 8, 9, 2, 4)
        self.conv3 = nn.Conv1d(8, 8, 9, 2, 4)

        with torch.no_grad():
            for param in self.parameters():
                param *= 4096

        if base is not None:
            self.load_state_dict(torch.load(base))

    def non_lin(self, x, floor):
        nn.functional.leaky_relu_(x, 1/64)
        x = (x / 4096.0).clamp(-4095, 4095)
        x = x.floor() if floor else x

        return x

    def forward(self, x, floor=False):
        x = self.conv1(x)
        x = self.non_lin(x, floor)

        x = self.conv2(x)
        x = self.non_lin(x, floor)

        x = self.conv3(x)
        x = self.non_lin(x, floor)

        return x

    def clamp_params(self):
        with torch.no_grad():
            for param in self.parameters():
                param.clamp_(-4095, 4095)
                param.floor_()

    def save(self, path):
        torch.save(self.state_dict(), path)
