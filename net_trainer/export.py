#!/usr/bin/env python3

import subprocess
import argparse
from net import LumosityNet
from jinja2 import Template

def arr2str(arr):
    if arr.dim() == 1:
        return '[' + ', '.join(str(int(e)) for e in arr) + ']'

    else:
        return '[' + ', '.join(arr2str(ln) for ln in arr) + ']'

def main():
    parser = argparse.ArgumentParser(description='Lumsity Exporter')

    parser.add_argument('-l', '--lumositynet', help='Network to export', required=True, type=str)
    parser.add_argument('-o', '--output', help='Destination file', required=True, type=str)

    args = parser.parse_args()

    network = LumosityNet(args.lumositynet)

    with open('node.tpl.rs') as fd:
        template = Template(fd.read())

    params = dict()

    for name, param in network.named_parameters():
        node, ntype = name.split('.')

        if node not in params:
            params[node] = dict()

        params[node][ntype] = param

    rust_prefix = 'extern crate liblumos; \npub use self::liblumos::Network;\n'

    rust = rust_prefix + '\n'.join(
        template.render(
            name_clean = name.upper(),
            weights = arr2str(node['weight']),
            bias = arr2str(node['bias']),
            outputs = node['weight'].shape[0],
            inputs = node['weight'].shape[1],
        )
        for name, node in params.items()
    )

    with open(args.output, 'w') as fd:
        subprocess.run(
            ['rustfmt'],
            input=rust.encode('ascii'),
            stdout=fd,
            check=True,
        )


if __name__ == '__main__':
    main()
