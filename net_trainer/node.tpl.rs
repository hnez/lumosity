pub static {{ name_clean }}: liblumos::ConvParamI{{ inputs }}O{{ outputs }} = liblumos::ConvParamI{{ inputs }}O{{ outputs }} {
    weights: {{ weights }},
    biases: {{ bias }},
};
