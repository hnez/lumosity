#!/usr/bin/env python3

import argparse
import itertools as it

import torch
import torch.nn as nn
import torch.optim as optim

from torch.utils.data import DataLoader

from net import LumosityNet
from dataset import LumosityDataset, SplitDataset

CATEGORIES = 'avada_kedavra cor finite_incantatem locomotor lumos nox stupify wingardium_leviosa'.split()

class Trainer(object):
    def __init__(self, dataset, network, device, batch_size):
        set_train, val_test_set = SplitDataset.split(dataset, 0.8, 4)
        set_val, set_test = SplitDataset.split(val_test_set, 0.8, 2)

        self.loader_train = DataLoader(set_train, batch_size=batch_size, shuffle=True, num_workers=1)
        self.loader_val = DataLoader(set_val, batch_size=batch_size)
        self.loader_test = DataLoader(set_test, batch_size=batch_size)

        self.network = network

        self.optimizer = optim.Adam(
            self.network.parameters(),
            lr=1.0
        )

        self.scheduler = optim.lr_scheduler.ReduceLROnPlateau(
            self.optimizer, mode='min',
            factor=0.25, patience=10,
        )

        self.device = device

        self.loss_fn = nn.MSELoss()

    def epoch(self):
        train_loss_acc = 0.0
        val_loss_acc = 0.0

        # Run through training set and update network
        for (train_batch_num, (sequences, labels)) in enumerate(self.loader_train):
            sequences = sequences.to(self.device)
            labels = labels.to(self.device)

            output = self.network.forward(sequences, False)
            loss = self.loss_fn(output, labels)

            del sequences
            del labels

            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

            train_loss = float(loss.detach())
            train_loss_acc += train_loss

            del loss

        self.network.clamp_params()

        # Run through validation set
        with torch.no_grad():
            global grab
            grab = list()

            for (val_batch_num, (sequences, labels)) in enumerate(self.loader_val):
                sequences = sequences.to(self.device)
                labels = labels.to(self.device)

                output = self.network.forward(sequences, True)
                loss = self.loss_fn(output, labels)

                grab.append(output.detach())

                del sequences
                del labels

                val_loss = float(loss.detach())
                val_loss_acc += val_loss

                del loss

        train_loss_avg = train_loss_acc / (train_batch_num + 1)
        val_loss_avg = val_loss_acc / (val_batch_num + 1)

        # Update learning rate
        self.scheduler.step(val_loss_avg)
        learn_rate = self.optimizer.param_groups[0]['lr']

        return (train_loss_avg, val_loss_avg, learn_rate)


def find_accelerator():
    use_cuda = torch.cuda.is_available()
    device = torch.device('cuda' if use_cuda else 'cpu')

    return device


def main():
    parser = argparse.ArgumentParser(description='Lumsity trainer')

    parser.add_argument('-d', '--dataset', help='Dataset Base', required=True, type=str)
    parser.add_argument('-l', '--lumositynet', help='Network to start training with', required=False, default=None, type=str)
    parser.add_argument('-b', '--batchsize', help='Batch size', required=False, default=10, type=int)

    args = parser.parse_args()

    device = find_accelerator()
    network = LumosityNet(args.lumositynet)
    dataset = LumosityDataset(args.dataset, CATEGORIES)

    print('Dataset length: {}'.format(len(dataset)))

    trainer = Trainer(dataset, network, device, args.batchsize)

    train_losses = list()
    val_losses = list()

    for epoch_num in it.count(1):
        train_loss, val_loss, learn_rate = trainer.epoch()

        train_losses.append(train_loss)
        val_losses.append(val_loss)

        best_train = train_loss <= min(train_losses)
        best_val = val_loss <= min(val_losses)

        if best_train:
            network.save('best_brain_train.pth')

        if best_val:
            network.save('best_brain_val.pth')

        print('e: {} t: {}{} v: {}{} l: {}'.format(
            epoch_num,
            int(train_loss), '*' if best_train else '',
            int(val_loss), '*' if best_val else '',
            learn_rate
        ))

        if learn_rate <= 1e-6:
                break

if __name__ == '__main__':
    main()
