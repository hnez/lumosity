use std::fs::File;
use std::io::prelude::*;

mod network;

fn read_dataset(filename: &str) -> Vec<[i16; 3]>{
    let lines = {
        let mut fd = File::open(filename).expect("file not found");
        let mut contents = String::new();

        fd.read_to_string(&mut contents).expect("something went wrong reading the file");

        contents
    };

    let dataset : Vec<_> = lines.lines().map(| line | {
        let mut fields = line.split(",").map(| l | (l.trim().parse::<f32>().unwrap() * 4096.0) as i16);

        let x = fields.next().unwrap();
        let y = fields.next().unwrap();
        let z = fields.next().unwrap();

        [x, y, z]
    }).collect();

    dataset
}


fn main() {
    let mut net = network::Network::new(&network::CONV1, &network::CONV2, &network::CONV3);

    for filename in std::env::args().skip(2) {
        println!("File {}:", filename);

        let dataset = read_dataset(&filename);

        let res : Vec<_> = dataset.into_iter().filter_map(|e| net.forward(e)).collect();

        for ln in res.iter() {
            println!("{:?}", ln);
        }
    }
}
