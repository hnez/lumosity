#![no_std]
#![feature(const_fn)]

pub struct ConvParamI3O8 {
    pub weights: [[[i16; 9]; 3]; 8],
    pub biases: [i16; 8],
}

struct ConvI3O8 {
    params: &'static ConvParamI3O8,
    storage: [[i16; 9]; 3],
    stcnt: u8,
}

impl ConvI3O8 {
    const fn new(params: &'static ConvParamI3O8) -> Self {
        ConvI3O8 {
            params: params,
            storage: [[0i16; 9]; 3],
            stcnt: 0,
        }
    }

    fn forward(&mut self, input: [i16; 3]) -> Option<[i16; 8]> {
        for idx_kern in 0..8 {
            for idx_in in 0..3 {
                self.storage[idx_in][idx_kern] = self.storage[idx_in][idx_kern + 1];
            }
        }

        for idx_in in 0..3 {
            self.storage[idx_in][8] = input[idx_in];
        }

        self.stcnt= (self.stcnt + 1) % 2;

        if self.stcnt == 0 {
            let mut dst = [0i16; 8];

            for idx_out in 0..8 {
                let mut acc = 0i32;

                for idx_kern in 0..9 {
                    for idx_in in 0..3 {
                        acc += (self.storage[idx_in][idx_kern] as i32) * (self.params.weights[idx_out][idx_in][idx_kern] as i32);
                    }
                }

                let acc = (acc >> 12) + (self.params.biases[idx_out] as i32); // Fixed point, bias
                let acc = if acc < 0 { acc / 64 } else { acc }; // LeakyReLu
                let acc = acc.max(-4095).min(4095); // Clamp

                dst[idx_out] = acc as i16;
            }

            Some(dst)
        }
        else {
            None
        }
    }
}

pub struct ConvParamI8O8 {
    pub weights: [[[i16; 9]; 8]; 8],
    pub biases: [i16; 8],
}

struct ConvI8O8 {
    params: &'static ConvParamI8O8,
    storage: [[i16; 9]; 8],
    stcnt: u8,
}

impl ConvI8O8 {
    const fn new(params: &'static ConvParamI8O8) -> Self {
        ConvI8O8 {
            params: params,
            storage: [[0i16; 9]; 8],
            stcnt: 0,
        }
    }

    fn forward(&mut self, input: [i16; 8]) -> Option<[i16; 8]> {
        for idx_kern in 0..8 {
            for idx_in in 0..8 {
                self.storage[idx_in][idx_kern] = self.storage[idx_in][idx_kern + 1];
            }
        }

        for idx_in in 0..8 {
            self.storage[idx_in][8] = input[idx_in];
        }

        self.stcnt= (self.stcnt + 1) % 2;

        if self.stcnt == 0 {
            let mut dst = [0i16; 8];

            for idx_out in 0..8 {
                let mut acc = 0i32;

                for idx_kern in 0..9 {
                    for idx_in in 0..8 {
                        acc += (self.storage[idx_in][idx_kern] as i32) * (self.params.weights[idx_out][idx_in][idx_kern] as i32);
                    }
                }

                let acc = (acc >> 12) + (self.params.biases[idx_out] as i32); // Fixed point, bias
                let acc = if acc < 0 { acc / 64 } else { acc }; // LeakyReLu
                let acc = acc.max(-4095).min(4095); // Clamp

                dst[idx_out] = acc as i16;
            }

            Some(dst)
        }
        else {
            None
        }
    }
}

pub struct Network {
    layer_1: ConvI3O8,
    layer_2: ConvI8O8,
    layer_3: ConvI8O8,
}

impl Network {
    pub const fn new(params_1: &'static ConvParamI3O8,
                 params_2: &'static ConvParamI8O8,
                 params_3: &'static ConvParamI8O8) -> Self {
        Network {
            layer_1: ConvI3O8::new(params_1),
            layer_2: ConvI8O8::new(params_2),
            layer_3: ConvI8O8::new(params_3),
        }
    }

    pub fn forward(&mut self, input: [i16; 3]) -> Option<[i16; 8]> {
        if let Some(l12) = self.layer_1.forward(input) {
            if let Some(l23) = self.layer_2.forward(l12) {
                if let Some(l3o) = self.layer_3.forward(l23) {
                    return Some(l3o)
                }
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn net_forward() {
        assert_eq!(2 + 2, 4);
    }
}
