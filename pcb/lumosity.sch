EESchema Schematic File Version 4
LIBS:lumosity-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L lumosity-rescue:R-device R10
U 1 1 5A940538
P 9350 4850
F 0 "R10" V 9430 4850 50  0000 C CNN
F 1 "1M" V 9350 4850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 9280 4850 50  0001 C CNN
F 3 "" H 9350 4850 50  0001 C CNN
	1    9350 4850
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:R-device R6
U 1 1 5A9406DA
P 8350 2800
F 0 "R6" V 8430 2800 50  0000 C CNN
F 1 "150R" V 8350 2800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8280 2800 50  0001 C CNN
F 3 "" H 8350 2800 50  0001 C CNN
	1    8350 2800
	0    1    1    0   
$EndComp
$Comp
L lumosity-rescue:R-device R7
U 1 1 5A940A73
P 8350 3000
F 0 "R7" V 8430 3000 50  0000 C CNN
F 1 "150R" V 8350 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8280 3000 50  0001 C CNN
F 3 "" H 8350 3000 50  0001 C CNN
	1    8350 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	10400 4850 10400 4700
Wire Wire Line
	10400 3950 10400 4100
Wire Wire Line
	10800 4400 11000 4400
$Comp
L lumosity-rescue:R-device R8
U 1 1 5A942278
P 9000 4100
F 0 "R8" V 8900 4100 50  0000 C CNN
F 1 "100k" V 9000 4100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8930 4100 50  0001 C CNN
F 3 "" H 9000 4100 50  0001 C CNN
	1    9000 4100
	-1   0    0    -1  
$EndComp
Text Label 9350 4300 0    60   ~ 0
VGND
Wire Wire Line
	9000 3900 9000 3950
Wire Wire Line
	9000 4300 10200 4300
$Comp
L lumosity-rescue:C-device C3
U 1 1 5A944029
P 9350 5450
F 0 "C3" H 9375 5550 50  0000 L CNN
F 1 "1n" H 9375 5350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 9388 5300 50  0001 C CNN
F 3 "" H 9350 5450 50  0001 C CNN
	1    9350 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	11000 5200 11000 4400
Wire Wire Line
	9350 5000 9350 5200
Wire Wire Line
	9000 4250 9000 4300
Connection ~ 9000 4300
Connection ~ 9350 5200
$Comp
L power:GND #PWR01
U 1 1 5A944C29
P 9350 6100
F 0 "#PWR01" H 9350 5850 50  0001 C CNN
F 1 "GND" H 9350 5950 50  0000 C CNN
F 2 "" H 9350 6100 50  0001 C CNN
F 3 "" H 9350 6100 50  0001 C CNN
	1    9350 6100
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:R-device R13
U 1 1 5A94553E
P 10400 5200
F 0 "R13" V 10480 5200 50  0000 C CNN
F 1 "4k3" V 10400 5200 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 10330 5200 50  0001 C CNN
F 3 "" H 10400 5200 50  0001 C CNN
	1    10400 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 5200 10150 5200
Wire Wire Line
	10550 5200 10700 5200
$Comp
L lumosity-rescue:C-device C4
U 1 1 5A945809
P 10400 5450
F 0 "C4" H 10425 5550 50  0000 L CNN
F 1 "1n" H 10425 5350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 10438 5300 50  0001 C CNN
F 3 "" H 10400 5450 50  0001 C CNN
	1    10400 5450
	0    1    1    0   
$EndComp
$Comp
L lumosity-rescue:R-device R11
U 1 1 5A9458A1
P 9350 5850
F 0 "R11" V 9430 5850 50  0000 C CNN
F 1 "4k3" V 9350 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 9280 5850 50  0001 C CNN
F 3 "" H 9350 5850 50  0001 C CNN
	1    9350 5850
	-1   0    0    1   
$EndComp
Wire Wire Line
	9350 6100 9350 6000
Wire Wire Line
	9350 5700 9350 5600
Wire Wire Line
	10250 5450 10150 5450
Wire Wire Line
	10150 5450 10150 5200
Connection ~ 10150 5200
Wire Wire Line
	10550 5450 10700 5450
Connection ~ 10700 5200
$Comp
L lumosity-rescue:74HC04-74xx U2
U 5 1 5A94636D
P 7550 2400
F 0 "U2" H 7700 2500 50  0000 C CNN
F 1 "74HC04" H 7750 2300 50  0000 C CNN
F 2 "SMD_Packages:SSOP-14" H 7550 2400 50  0001 C CNN
F 3 "" H 7550 2400 50  0001 C CNN
	5    7550 2400
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:74HC04-74xx U2
U 6 1 5A94656E
P 7550 3250
F 0 "U2" H 7700 3350 50  0000 C CNN
F 1 "74HC04" H 7750 3150 50  0000 C CNN
F 2 "SMD_Packages:SSOP-14" H 7550 3250 50  0001 C CNN
F 3 "" H 7550 3250 50  0001 C CNN
	6    7550 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR02
U 1 1 5A9461AD
P 10400 3950
F 0 "#PWR02" H 10400 3800 50  0001 C CNN
F 1 "+3.3V" H 10400 4090 50  0000 C CNN
F 2 "" H 10400 3950 50  0001 C CNN
F 3 "" H 10400 3950 50  0001 C CNN
	1    10400 3950
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:LED_RGB-device D6
U 1 1 5A9466A7
P 12800 2400
F 0 "D6" H 12800 2770 50  0000 C CNN
F 1 "LED_RGB" H 12800 2050 50  0000 C CNN
F 2 "custom:led_19_337" H 12800 2350 50  0001 C CNN
F 3 "" H 12800 2350 50  0001 C CNN
	1    12800 2400
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:74HC04-74xx U2
U 1 1 5A946785
P 7550 2800
F 0 "U2" H 7700 2900 50  0000 C CNN
F 1 "74HC04" H 7750 2700 50  0000 C CNN
F 2 "SMD_Packages:SSOP-14" H 7550 2800 50  0001 C CNN
F 3 "" H 7550 2800 50  0001 C CNN
	1    7550 2800
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:74HC04-74xx U2
U 2 1 5A946800
P 7550 1900
F 0 "U2" H 7700 2000 50  0000 C CNN
F 1 "74HC04" H 7750 1800 50  0000 C CNN
F 2 "SMD_Packages:SSOP-14" H 7550 1900 50  0001 C CNN
F 3 "" H 7550 1900 50  0001 C CNN
	2    7550 1900
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:74HC04-74xx U2
U 3 1 5A946860
P 7550 1100
F 0 "U2" H 7700 1200 50  0000 C CNN
F 1 "74HC04" H 7750 1000 50  0000 C CNN
F 2 "SMD_Packages:SSOP-14" H 7550 1100 50  0001 C CNN
F 3 "" H 7550 1100 50  0001 C CNN
	3    7550 1100
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:R-device R2
U 1 1 5A9469C5
P 8350 1500
F 0 "R2" V 8430 1500 50  0000 C CNN
F 1 "68R" V 8350 1500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8280 1500 50  0001 C CNN
F 3 "" H 8350 1500 50  0001 C CNN
	1    8350 1500
	0    1    1    0   
$EndComp
$Comp
L lumosity-rescue:R-device R1
U 1 1 5A946A16
P 8350 1300
F 0 "R1" V 8430 1300 50  0000 C CNN
F 1 "150R" V 8350 1300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8280 1300 50  0001 C CNN
F 3 "" H 8350 1300 50  0001 C CNN
	1    8350 1300
	0    1    1    0   
$EndComp
$Comp
L lumosity-rescue:R-device R3
U 1 1 5A946A6D
P 8350 1700
F 0 "R3" V 8430 1700 50  0000 C CNN
F 1 "68R" V 8350 1700 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8280 1700 50  0001 C CNN
F 3 "" H 8350 1700 50  0001 C CNN
	1    8350 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 1500 8200 1500
Wire Wire Line
	8000 1900 8100 1900
Wire Wire Line
	8100 1900 8100 1700
Wire Wire Line
	8100 1700 8200 1700
Wire Wire Line
	8000 1100 8100 1100
Wire Wire Line
	8100 1100 8100 1300
Wire Wire Line
	8100 1300 8200 1300
$Comp
L lumosity-rescue:LED-device D4
U 1 1 5A946E4C
P 12850 3250
F 0 "D4" H 12850 3350 50  0000 C CNN
F 1 "White" H 12650 3350 50  0000 C CNN
F 2 "LEDs:LED_0603" H 12850 3250 50  0001 C CNN
F 3 "" H 12850 3250 50  0001 C CNN
	1    12850 3250
	1    0    0    1   
$EndComp
$Comp
L lumosity-rescue:74HC04-74xx U2
U 4 1 5A946FC2
P 7550 1500
F 0 "U2" H 7700 1600 50  0000 C CNN
F 1 "74HC04" H 7750 1400 50  0000 C CNN
F 2 "SMD_Packages:SSOP-14" H 7550 1500 50  0001 C CNN
F 3 "" H 7550 1500 50  0001 C CNN
	4    7550 1500
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:R-device R4
U 1 1 5A947041
P 8350 2300
F 0 "R4" V 8430 2300 50  0000 C CNN
F 1 "150R" V 8350 2300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8280 2300 50  0001 C CNN
F 3 "" H 8350 2300 50  0001 C CNN
	1    8350 2300
	0    1    1    0   
$EndComp
$Comp
L lumosity-rescue:R-device R5
U 1 1 5A9470D3
P 8350 2500
F 0 "R5" V 8430 2500 50  0000 C CNN
F 1 "150R" V 8350 2500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8280 2500 50  0001 C CNN
F 3 "" H 8350 2500 50  0001 C CNN
	1    8350 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 2400 8100 2400
Wire Wire Line
	8100 2500 8200 2500
Connection ~ 8100 2400
Wire Wire Line
	8100 2300 8200 2300
$Comp
L lumosity-rescue:LED-device D5
U 1 1 5A944B4D
P 12850 3000
F 0 "D5" H 12850 3100 50  0000 C CNN
F 1 "White" H 12650 3100 50  0000 C CNN
F 2 "LEDs:LED_0603" H 12850 3000 50  0001 C CNN
F 3 "" H 12850 3000 50  0001 C CNN
	1    12850 3000
	1    0    0    1   
$EndComp
$Comp
L lumosity-rescue:LED-device D2
U 1 1 5A944BBB
P 11000 2900
F 0 "D2" H 11000 3000 50  0000 C CNN
F 1 "IR 19-21C" V 11300 2900 50  0000 C CNN
F 2 "LEDs:LED_0603" H 11000 2900 50  0001 C CNN
F 3 "" H 11000 2900 50  0001 C CNN
	1    11000 2900
	0    -1   1    0   
$EndComp
$Comp
L lumosity-rescue:LED-device D3
U 1 1 5A944C67
P 11450 2900
F 0 "D3" H 11450 3000 50  0000 C CNN
F 1 "IR 19-21C" V 11750 2900 50  0000 C CNN
F 2 "LEDs:LED_0603" H 11450 2900 50  0001 C CNN
F 3 "" H 11450 2900 50  0001 C CNN
	1    11450 2900
	0    -1   1    0   
$EndComp
Wire Wire Line
	13300 1800 13300 2200
Wire Wire Line
	13000 2600 13300 2600
Wire Wire Line
	13300 2200 13000 2200
Wire Wire Line
	13000 2400 13300 2400
Connection ~ 13300 2400
Connection ~ 13300 2600
Connection ~ 13300 3000
Connection ~ 13300 2200
$Comp
L lumosity-rescue:Conn_01x10-conn J5
U 1 1 5A945669
P 9650 2400
F 0 "J5" H 9650 2900 50  0000 C CNN
F 1 "Conn_01x10" H 9650 1800 50  0001 C CNN
F 2 "custom:con_mn_ld" H 9650 2400 50  0001 C CNN
F 3 "" H 9650 2400 50  0001 C CNN
	1    9650 2400
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:Conn_01x10-conn J6
U 1 1 5A9457AB
P 9950 2400
F 0 "J6" H 9950 2900 50  0000 C CNN
F 1 "Conn_01x10" H 9950 1800 50  0001 C CNN
F 2 "custom:conn_ld_mn" H 9950 2400 50  0001 C CNN
F 3 "" H 9950 2400 50  0001 C CNN
	1    9950 2400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8100 2300 8100 2400
$Comp
L lumosity-rescue:R-device R9
U 1 1 5A946E36
P 9000 4500
F 0 "R9" V 8900 4500 50  0000 C CNN
F 1 "100k" V 9000 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 8930 4500 50  0001 C CNN
F 3 "" H 9000 4500 50  0001 C CNN
	1    9000 4500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8000 2800 8100 2800
Wire Wire Line
	8200 3000 8100 3000
Wire Wire Line
	8100 3000 8100 2800
Connection ~ 8100 2800
Text Label 6950 2800 2    60   ~ 0
~IR_RCVEN
Wire Wire Line
	6950 2800 7100 2800
Text Label 6950 3250 2    60   ~ 0
~IR_ON
Wire Wire Line
	6950 3250 7100 3250
$Comp
L power:+3.3V #PWR03
U 1 1 5A947EDA
P 9000 3900
F 0 "#PWR03" H 9000 3750 50  0001 C CNN
F 1 "+3.3V" H 9000 4040 50  0000 C CNN
F 2 "" H 9000 3900 50  0001 C CNN
F 3 "" H 9000 3900 50  0001 C CNN
	1    9000 3900
	1    0    0    -1  
$EndComp
Text Label 8800 4750 2    60   ~ 0
~IR_RCVEN
Wire Wire Line
	8800 4750 9000 4750
Wire Wire Line
	9000 4750 9000 4650
Text Label 8600 3250 0    60   ~ 0
IR_FB
Text Label 9350 4500 0    60   ~ 0
IR_FB
Wire Wire Line
	9350 4700 9350 4500
Text Label 6950 2400 2    60   ~ 0
LED_WHITE
Wire Wire Line
	6950 2400 7100 2400
Text Label 6950 1100 2    60   ~ 0
LED_R
Wire Wire Line
	6950 1100 7100 1100
$Comp
L power:VCC #PWR04
U 1 1 5A948BB6
P 9350 1900
F 0 "#PWR04" H 9350 1750 50  0001 C CNN
F 1 "VCC" H 9350 2050 50  0000 C CNN
F 2 "" H 9350 1900 50  0001 C CNN
F 3 "" H 9350 1900 50  0001 C CNN
	1    9350 1900
	1    0    0    -1  
$EndComp
Text Label 6950 1500 2    60   ~ 0
LED_G
Text Label 6950 1900 2    60   ~ 0
LED_B
Wire Wire Line
	6950 1900 7100 1900
Wire Wire Line
	6950 1500 7100 1500
Wire Wire Line
	10700 5450 10700 5200
$Comp
L power:VCC #PWR05
U 1 1 5A949857
P 4300 3250
F 0 "#PWR05" H 4300 3100 50  0001 C CNN
F 1 "VCC" H 4300 3400 50  0000 C CNN
F 2 "" H 4300 3250 50  0001 C CNN
F 3 "" H 4300 3250 50  0001 C CNN
	1    4300 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5A949955
P 4500 4400
F 0 "#PWR06" H 4500 4150 50  0001 C CNN
F 1 "GND" H 4500 4250 50  0000 C CNN
F 2 "" H 4500 4400 50  0001 C CNN
F 3 "" H 4500 4400 50  0001 C CNN
	1    4500 4400
	1    0    0    -1  
$EndComp
Text Label 11150 4400 0    60   ~ 0
IR_IN
Connection ~ 11000 4400
$Comp
L power:VCC #PWR07
U 1 1 5A94A6FB
P 3850 1000
F 0 "#PWR07" H 3850 850 50  0001 C CNN
F 1 "VCC" H 3850 1150 50  0000 C CNN
F 2 "" H 3850 1000 50  0001 C CNN
F 3 "" H 3850 1000 50  0001 C CNN
	1    3850 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR08
U 1 1 5A94A7B2
P 5000 1000
F 0 "#PWR08" H 5000 850 50  0001 C CNN
F 1 "+3.3V" H 5000 1140 50  0000 C CNN
F 2 "" H 5000 1000 50  0001 C CNN
F 3 "" H 5000 1000 50  0001 C CNN
	1    5000 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5A94ABEA
P 4400 1950
F 0 "#PWR09" H 4400 1700 50  0001 C CNN
F 1 "GND" H 4400 1800 50  0000 C CNN
F 2 "" H 4400 1950 50  0001 C CNN
F 3 "" H 4400 1950 50  0001 C CNN
	1    4400 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 4500 10200 4500
Wire Wire Line
	9350 4500 9750 4500
$Comp
L lumosity-rescue:R-device R12
U 1 1 5A94B35B
P 9900 4500
F 0 "R12" V 9800 4500 50  0000 C CNN
F 1 "100k" V 9900 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 9830 4500 50  0001 C CNN
F 3 "" H 9900 4500 50  0001 C CNN
	1    9900 4500
	0    1    -1   0   
$EndComp
$Comp
L lumosity-rescue:Conn_01x10-conn J4
U 1 1 5A94BB9D
P 3900 3750
F 0 "J4" H 3900 4250 50  0000 C CNN
F 1 "Conn_01x10" H 3900 3150 50  0001 C CNN
F 2 "custom:con_mn_ld" H 3900 3750 50  0001 C CNN
F 3 "" H 3900 3750 50  0001 C CNN
	1    3900 3750
	-1   0    0    -1  
$EndComp
$Comp
L lumosity-rescue:Conn_01x10-conn J3
U 1 1 5A94BC58
P 3700 3750
F 0 "J3" H 3700 4250 50  0000 C CNN
F 1 "Conn_01x10" H 3700 3150 50  0001 C CNN
F 2 "custom:conn_ld_mn" H 3700 3750 50  0001 C CNN
F 3 "" H 3700 3750 50  0001 C CNN
	1    3700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 4250 4100 4250
Wire Wire Line
	4300 3250 4300 3350
Wire Wire Line
	4100 3750 4300 3750
Connection ~ 4300 3750
Wire Wire Line
	4100 3350 4300 3350
Connection ~ 4300 3350
Wire Wire Line
	4100 3850 4300 3850
Connection ~ 4300 3850
Wire Wire Line
	4100 3450 4500 3450
Wire Wire Line
	4500 3450 4500 3550
Wire Wire Line
	4100 3550 4500 3550
Connection ~ 4500 3550
Wire Wire Line
	4100 3650 4500 3650
Connection ~ 4500 3650
Wire Wire Line
	4100 3950 4500 3950
Connection ~ 4500 3950
Wire Wire Line
	4100 4050 4500 4050
Connection ~ 4500 4050
Wire Wire Line
	4100 4150 4500 4150
Connection ~ 4500 4150
$Comp
L lumosity-rescue:Conn_01x01-conn J2
U 1 1 5A94CE17
P 3250 3050
F 0 "J2" H 3250 3150 50  0000 C CNN
F 1 "BAT+" H 3250 2950 50  0000 C CNN
F 2 "custom:BAT_P" H 3250 3050 50  0001 C CNN
F 3 "" H 3250 3050 50  0001 C CNN
	1    3250 3050
	0    -1   -1   0   
$EndComp
$Comp
L lumosity-rescue:Conn_01x01-conn J1
U 1 1 5A94CFA7
P 1950 4550
F 0 "J1" H 1950 4650 50  0000 C CNN
F 1 "BAT-" H 1950 4450 50  0000 C CNN
F 2 "custom:BAT_N" H 1950 4550 50  0001 C CNN
F 3 "" H 1950 4550 50  0001 C CNN
	1    1950 4550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3250 4250 3500 4250
Wire Wire Line
	3250 3250 3250 3350
Wire Wire Line
	3500 3850 3250 3850
Connection ~ 3250 3850
Wire Wire Line
	3250 3750 3500 3750
Connection ~ 3250 3750
Wire Wire Line
	3500 3350 3250 3350
Connection ~ 3250 3350
Wire Wire Line
	3500 3450 3050 3450
Wire Wire Line
	3050 3450 3050 3550
Wire Wire Line
	3500 3550 3050 3550
Connection ~ 3050 3550
Wire Wire Line
	3500 3650 3050 3650
Connection ~ 3050 3650
Wire Wire Line
	3500 3950 3050 3950
Connection ~ 3050 3950
Wire Wire Line
	3500 4050 3050 4050
Connection ~ 3050 4050
Wire Wire Line
	3500 4150 3050 4150
Connection ~ 3050 4150
Wire Wire Line
	9450 2700 9350 2700
Wire Wire Line
	9350 2700 9350 1900
Wire Wire Line
	10150 2700 10300 2700
Wire Wire Line
	10300 2700 10300 1800
Wire Wire Line
	10300 1800 13300 1800
Wire Wire Line
	10150 2200 12600 2200
Wire Wire Line
	10150 2300 12500 2300
Wire Wire Line
	12500 2300 12500 2400
Wire Wire Line
	12500 2400 12600 2400
Wire Wire Line
	10150 2100 12400 2100
Wire Wire Line
	12400 2100 12400 2600
Wire Wire Line
	12400 2600 12600 2600
Wire Wire Line
	13000 3000 13300 3000
Wire Wire Line
	13300 3250 13000 3250
Wire Wire Line
	12700 3000 12150 3000
Wire Wire Line
	12150 3250 12700 3250
Wire Wire Line
	10150 2600 11000 2600
Wire Wire Line
	11000 2600 11000 2750
Wire Wire Line
	10150 2800 10800 2800
Wire Wire Line
	10800 2800 10800 3150
Wire Wire Line
	10800 3150 11000 3150
Wire Wire Line
	11000 3150 11000 3050
Wire Wire Line
	11450 2000 11450 2750
Wire Wire Line
	10700 2400 10700 3300
Wire Wire Line
	10700 3300 11450 3300
Wire Wire Line
	11450 3300 11450 3050
Wire Wire Line
	10150 2400 10700 2400
Wire Wire Line
	10150 2900 10400 2900
Wire Wire Line
	10400 2900 10400 3400
Wire Wire Line
	10400 3400 12150 3400
Wire Wire Line
	12150 3400 12150 3250
Wire Wire Line
	10150 2000 11450 2000
Wire Wire Line
	10150 2500 12150 2500
Wire Wire Line
	12150 2500 12150 3000
Wire Wire Line
	9450 2200 9100 2200
Wire Wire Line
	9100 2200 9100 1300
Wire Wire Line
	9100 1300 8500 1300
Wire Wire Line
	9450 2300 9000 2300
Wire Wire Line
	9000 2300 9000 1500
Wire Wire Line
	9000 1500 8500 1500
Wire Wire Line
	9450 2100 8900 2100
Wire Wire Line
	8900 2100 8900 1700
Wire Wire Line
	8900 1700 8500 1700
Wire Wire Line
	9450 2500 8900 2500
Wire Wire Line
	8900 2500 8900 2300
Wire Wire Line
	8900 2300 8500 2300
Wire Wire Line
	8500 2500 8800 2500
Wire Wire Line
	8800 2500 8800 2900
Wire Wire Line
	8800 2900 9450 2900
Wire Wire Line
	8500 2800 8950 2800
Wire Wire Line
	8950 2800 8950 2600
Wire Wire Line
	8950 2600 9450 2600
Wire Wire Line
	8500 3000 8700 3000
Wire Wire Line
	8700 3000 8700 2000
Wire Wire Line
	8700 2000 9450 2000
Wire Wire Line
	9200 3250 9200 2800
Wire Wire Line
	9200 2400 9450 2400
Wire Wire Line
	9450 2800 9200 2800
Connection ~ 9200 2800
$Comp
L custom:ADXL345 U4
U 1 1 5A955B75
P 8000 8750
F 0 "U4" H 8250 9350 60  0000 C CNN
F 1 "ADXL345" H 7600 9350 60  0000 C CNN
F 2 "custom:ADXL345" H 8000 8750 60  0001 C CNN
F 3 "" H 8000 8750 60  0001 C CNN
	1    8000 8750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5A955E30
P 8000 9650
F 0 "#PWR010" H 8000 9400 50  0001 C CNN
F 1 "GND" H 8000 9500 50  0000 C CNN
F 2 "" H 8000 9650 50  0001 C CNN
F 3 "" H 8000 9650 50  0001 C CNN
	1    8000 9650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 9450 7900 9550
Wire Wire Line
	7900 9550 8000 9550
Wire Wire Line
	8100 9550 8100 9450
Wire Wire Line
	8000 9450 8000 9550
Connection ~ 8000 9550
$Comp
L power:+3.3V #PWR011
U 1 1 5A9563CE
P 8000 7850
F 0 "#PWR011" H 8000 7700 50  0001 C CNN
F 1 "+3.3V" H 8000 7990 50  0000 C CNN
F 2 "" H 8000 7850 50  0001 C CNN
F 3 "" H 8000 7850 50  0001 C CNN
	1    8000 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 8050 7950 7950
Wire Wire Line
	7950 7950 8000 7950
Wire Wire Line
	8050 7950 8050 8050
Wire Wire Line
	8000 7950 8000 7850
Connection ~ 8000 7950
$Comp
L custom:STM32F030F4 U5
U 1 1 5A95742D
P 2850 9050
F 0 "U5" H 3200 10000 60  0000 C CNN
F 1 "STM32F030F4" H 2250 10000 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-20_4.4x6.5mm_Pitch0.65mm" H 3450 8950 60  0001 C CNN
F 3 "" H 3450 8950 60  0001 C CNN
	1    2850 9050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR012
U 1 1 5A9575EB
P 2850 7750
F 0 "#PWR012" H 2850 7600 50  0001 C CNN
F 1 "+3.3V" H 2850 7890 50  0000 C CNN
F 2 "" H 2850 7750 50  0001 C CNN
F 3 "" H 2850 7750 50  0001 C CNN
	1    2850 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 7950 2750 7850
Wire Wire Line
	2750 7850 2850 7850
Wire Wire Line
	2950 7850 2950 7950
Wire Wire Line
	2850 7850 2850 7750
Connection ~ 2850 7850
$Comp
L power:GND #PWR013
U 1 1 5A957A03
P 2850 10150
F 0 "#PWR013" H 2850 9900 50  0001 C CNN
F 1 "GND" H 2850 10000 50  0000 C CNN
F 2 "" H 2850 10150 50  0001 C CNN
F 3 "" H 2850 10150 50  0001 C CNN
	1    2850 10150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 9950 2850 10050
$Comp
L lumosity-rescue:Crystal-device Y1
U 1 1 5A957C9B
P 1850 8850
F 0 "Y1" H 1850 9000 50  0000 C CNN
F 1 "8MHz" H 1850 8700 50  0000 C CNN
F 2 "Crystals:Crystal_SMD_HC49-SD" H 1850 8850 50  0001 C CNN
F 3 "" H 1850 8850 50  0001 C CNN
	1    1850 8850
	0    1    1    0   
$EndComp
Wire Wire Line
	7350 8850 7450 8850
Text Label 4150 8350 0    60   ~ 0
ACCEL_INT
Wire Wire Line
	4150 8850 3500 8850
Text Label 4150 9250 0    60   ~ 0
ACCEL_SDA
Text Label 4150 9150 0    60   ~ 0
ACCEL_SCL
Wire Wire Line
	3500 9150 4150 9150
Wire Wire Line
	4150 9250 3500 9250
Text Label 7350 8850 2    60   ~ 0
ACCEL_INT
Text Label 8650 8500 0    60   ~ 0
ACCEL_SCL
Wire Wire Line
	8550 8500 9300 8500
Text Label 8650 8600 0    60   ~ 0
ACCEL_SDA
Wire Wire Line
	8550 8600 9500 8600
Text Label 4150 8650 0    60   ~ 0
DBG_RX
Text Label 4150 8550 0    60   ~ 0
DBG_TX
Wire Wire Line
	3500 8550 4150 8550
Wire Wire Line
	4150 8650 3500 8650
Text Label 4150 9350 0    60   ~ 0
SWD_DIO
Wire Wire Line
	3500 9350 4800 9350
Text Label 4150 9450 0    60   ~ 0
SWD_CLK
Wire Wire Line
	3500 9450 4150 9450
Text Label 4800 9350 0    60   ~ 0
~IR_ON
Text Label 4150 8750 0    60   ~ 0
LED_WHITE
Wire Wire Line
	4150 9050 3500 9050
Text Label 4150 9050 0    60   ~ 0
LED_R
Text Label 4150 9650 0    60   ~ 0
LED_G
Wire Wire Line
	4150 8950 3500 8950
Text Label 4150 8950 0    60   ~ 0
LED_B
Wire Wire Line
	4150 9650 3500 9650
Wire Wire Line
	3500 8750 4150 8750
Text Label 4150 8850 0    60   ~ 0
~IR_RCVEN
Wire Wire Line
	4150 8350 3500 8350
Text Label 4150 8450 0    60   ~ 0
IR_IN
Wire Wire Line
	4150 8450 3500 8450
NoConn ~ 2050 9200
Wire Wire Line
	2050 9200 2200 9200
Wire Wire Line
	2200 9300 2050 9300
$Comp
L lumosity-rescue:Conn_01x04-conn J8
U 1 1 5A95BC5C
P 7350 10500
F 0 "J8" H 7350 10700 50  0000 C CNN
F 1 "SWD" H 7350 10200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch1.27mm" H 7350 10500 50  0001 C CNN
F 3 "" H 7350 10500 50  0001 C CNN
	1    7350 10500
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:Conn_01x04-conn J7
U 1 1 5A95BDD7
P 6500 10500
F 0 "J7" H 6500 10700 50  0000 C CNN
F 1 "UART" H 6500 10200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch1.27mm" H 6500 10500 50  0001 C CNN
F 3 "" H 6500 10500 50  0001 C CNN
	1    6500 10500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5A95C084
P 6200 10800
F 0 "#PWR014" H 6200 10550 50  0001 C CNN
F 1 "GND" H 6200 10650 50  0000 C CNN
F 2 "" H 6200 10800 50  0001 C CNN
F 3 "" H 6200 10800 50  0001 C CNN
	1    6200 10800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 10800 6200 10700
Wire Wire Line
	6200 10700 6300 10700
Text Label 6200 10600 2    60   ~ 0
DBG_TX
Wire Wire Line
	6200 10600 6300 10600
Text Label 6200 10500 2    60   ~ 0
DBG_RX
Wire Wire Line
	6200 10500 6300 10500
$Comp
L power:+3.3V #PWR015
U 1 1 5A95C702
P 6200 10350
F 0 "#PWR015" H 6200 10200 50  0001 C CNN
F 1 "+3.3V" H 6200 10490 50  0000 C CNN
F 2 "" H 6200 10350 50  0001 C CNN
F 3 "" H 6200 10350 50  0001 C CNN
	1    6200 10350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 10350 6200 10400
Wire Wire Line
	6200 10400 6300 10400
$Comp
L power:GND #PWR016
U 1 1 5A95C99A
P 7050 10800
F 0 "#PWR016" H 7050 10550 50  0001 C CNN
F 1 "GND" H 7050 10650 50  0000 C CNN
F 2 "" H 7050 10800 50  0001 C CNN
F 3 "" H 7050 10800 50  0001 C CNN
	1    7050 10800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 10800 7050 10700
Wire Wire Line
	7050 10700 7150 10700
$Comp
L power:+3.3V #PWR017
U 1 1 5A95CB26
P 7050 10300
F 0 "#PWR017" H 7050 10150 50  0001 C CNN
F 1 "+3.3V" H 7050 10440 50  0000 C CNN
F 2 "" H 7050 10300 50  0001 C CNN
F 3 "" H 7050 10300 50  0001 C CNN
	1    7050 10300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 10300 7050 10400
Wire Wire Line
	7050 10400 7150 10400
Text Label 7050 10600 2    60   ~ 0
SWD_CLK
Wire Wire Line
	7050 10600 7150 10600
Text Label 7050 10500 2    60   ~ 0
SWD_DIO
Wire Wire Line
	7050 10500 7150 10500
$Comp
L lumosity-rescue:C-device C5
U 1 1 5A95DB67
P 1450 8650
F 0 "C5" V 1300 8500 50  0000 L CNN
F 1 "22p" V 1300 8750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1488 8500 50  0001 C CNN
F 3 "" H 1450 8650 50  0001 C CNN
	1    1450 8650
	0    1    1    0   
$EndComp
$Comp
L lumosity-rescue:C-device C6
U 1 1 5A95DE11
P 1450 9050
F 0 "C6" V 1300 8900 50  0000 L CNN
F 1 "22p" V 1300 9150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1488 8900 50  0001 C CNN
F 3 "" H 1450 9050 50  0001 C CNN
	1    1450 9050
	0    1    -1   0   
$EndComp
Wire Wire Line
	1600 9050 1850 9050
Wire Wire Line
	1850 9050 1850 9000
Wire Wire Line
	1600 8650 1850 8650
Wire Wire Line
	1850 8650 1850 8700
Wire Wire Line
	2100 8650 2100 8800
Wire Wire Line
	2100 8800 2200 8800
Connection ~ 1850 8650
Wire Wire Line
	2150 9050 2150 8900
Wire Wire Line
	2150 8900 2200 8900
Connection ~ 1850 9050
$Comp
L power:GND #PWR018
U 1 1 5A95E424
P 1150 9250
F 0 "#PWR018" H 1150 9000 50  0001 C CNN
F 1 "GND" H 1150 9100 50  0000 C CNN
F 2 "" H 1150 9250 50  0001 C CNN
F 3 "" H 1150 9250 50  0001 C CNN
	1    1150 9250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 8650 1150 9050
Wire Wire Line
	1150 8650 1300 8650
Wire Wire Line
	1300 9050 1150 9050
Connection ~ 1150 9050
Wire Wire Line
	4400 1750 4400 1850
Wire Wire Line
	3850 1000 3850 1100
Wire Wire Line
	3850 1250 4000 1250
Wire Wire Line
	3700 1100 3850 1100
Connection ~ 3850 1100
Wire Wire Line
	4800 1100 5000 1100
$Comp
L custom:TPS73633 U1
U 1 1 5A95FD3C
P 4400 1200
F 0 "U1" H 4050 1550 60  0000 C CNN
F 1 "TLV 70033" H 4500 1550 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 4400 1200 60  0001 C CNN
F 3 "" H 4400 1200 60  0001 C CNN
F 4 "TLV 70033 DDCT" H 4400 1200 60  0001 C CNN "Reichelt"
	1    4400 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1100 5000 1000
NoConn ~ 4950 1250
Wire Wire Line
	4800 1250 4950 1250
$Comp
L custom:MCP6401 U3
U 1 1 5A960603
P 10500 4400
F 0 "U3" H 10500 4700 50  0000 L CNN
F 1 "MCP6401" H 10500 4600 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 10500 4400 50  0001 C CNN
F 3 "" H 10500 4400 50  0001 C CNN
F 4 "MCP 6401T-E/OT" H 10500 4400 60  0001 C CNN "Reichelt"
	1    10500 4400
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:C-device C7
U 1 1 5A9613BE
P 6300 7850
F 0 "C7" H 6325 7950 50  0000 L CNN
F 1 "100n" H 6325 7750 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6338 7700 50  0001 C CNN
F 3 "" H 6300 7850 50  0001 C CNN
	1    6300 7850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5A9614B7
P 6300 8100
F 0 "#PWR019" H 6300 7850 50  0001 C CNN
F 1 "GND" H 6300 7950 50  0000 C CNN
F 2 "" H 6300 8100 50  0001 C CNN
F 3 "" H 6300 8100 50  0001 C CNN
	1    6300 8100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 8100 6300 8000
$Comp
L power:+3.3V #PWR020
U 1 1 5A9616A8
P 6300 7600
F 0 "#PWR020" H 6300 7450 50  0001 C CNN
F 1 "+3.3V" H 6300 7740 50  0000 C CNN
F 2 "" H 6300 7600 50  0001 C CNN
F 3 "" H 6300 7600 50  0001 C CNN
	1    6300 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 7600 6300 7700
Text Label 10250 4850 2    60   ~ 0
~IR_RCVEN
Wire Wire Line
	10250 4850 10400 4850
$Comp
L lumosity-rescue:C-device C2
U 1 1 5A96AB24
P 5150 1400
F 0 "C2" H 5175 1500 50  0000 L CNN
F 1 "100n" H 5175 1300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 5188 1250 50  0001 C CNN
F 3 "" H 5150 1400 50  0001 C CNN
	1    5150 1400
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:C-device C1
U 1 1 5A96AD76
P 3700 1450
F 0 "C1" H 3725 1550 50  0000 L CNN
F 1 "100n" H 3725 1350 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3738 1300 50  0001 C CNN
F 3 "" H 3700 1450 50  0001 C CNN
	1    3700 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1100 5150 1250
Connection ~ 5000 1100
Wire Wire Line
	3700 1300 3700 1100
Wire Wire Line
	3700 1600 3700 1850
Wire Wire Line
	3700 1850 4400 1850
Wire Wire Line
	5150 1850 5150 1550
Connection ~ 4400 1850
$Comp
L power:+3.3V #PWR021
U 1 1 5A96CC13
P 9750 7700
F 0 "#PWR021" H 9750 7550 50  0001 C CNN
F 1 "+3.3V" H 9750 7840 50  0000 C CNN
F 2 "" H 9750 7700 50  0001 C CNN
F 3 "" H 9750 7700 50  0001 C CNN
	1    9750 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9750 8950 8700 8950
Wire Wire Line
	9750 7700 9750 7900
Wire Wire Line
	8550 8850 8700 8850
Wire Wire Line
	8700 8850 8700 8950
Connection ~ 8700 8950
Text Notes 3600 8350 0    60   ~ 0
WKUP1
Text Notes 3600 8550 0    60   ~ 0
USART1_TX
Text Notes 3600 8650 0    60   ~ 0
USART1_RX
Text Notes 3600 8750 0    60   ~ 0
TIM14_CH1
Text Notes 3600 8950 0    60   ~ 0
TIM3_CH1
Text Notes 3600 9050 0    60   ~ 0
TIM3_CH2
Text Notes 3600 9150 0    60   ~ 0
I2C1_SCL
Text Notes 3600 9250 0    60   ~ 0
I2C1_SDA
Text Notes 3600 9350 0    60   ~ 0
IR_OUT
Wire Wire Line
	2050 9300 2050 10050
Wire Wire Line
	2050 10050 2850 10050
Connection ~ 2850 10050
Text Notes 3600 9650 0    60   ~ 0
TIM3_CH4
$Comp
L lumosity-rescue:D_Schottky_x2_Serial_AKC-device D1
U 1 1 5A96F6AA
P 8500 3550
F 0 "D1" H 8550 3450 50  0000 C CNN
F 1 "BAR43S" H 8500 3650 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 8500 3550 50  0001 C CNN
F 3 "" H 8500 3550 50  0001 C CNN
F 4 "BAR 43S SMD" H 8500 3550 60  0001 C CNN "Reichelt"
	1    8500 3550
	1    0    0    1   
$EndComp
Wire Wire Line
	8000 3250 8100 3250
Wire Wire Line
	8100 3250 8100 3550
Wire Wire Line
	8100 3550 8200 3550
Wire Wire Line
	8500 3350 8500 3250
Wire Wire Line
	8500 3250 8900 3250
Wire Wire Line
	8800 3550 8900 3550
Wire Wire Line
	8900 3550 8900 3250
Connection ~ 8900 3250
$Comp
L lumosity-rescue:R-device R14
U 1 1 5A96C10F
P 9300 8150
F 0 "R14" V 9380 8150 50  0000 C CNN
F 1 "4k7" V 9300 8150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 9230 8150 50  0001 C CNN
F 3 "" H 9300 8150 50  0001 C CNN
	1    9300 8150
	1    0    0    -1  
$EndComp
$Comp
L lumosity-rescue:R-device R15
U 1 1 5A96C397
P 9500 8150
F 0 "R15" V 9580 8150 50  0000 C CNN
F 1 "4k7" V 9500 8150 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 9430 8150 50  0001 C CNN
F 3 "" H 9500 8150 50  0001 C CNN
	1    9500 8150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 8500 9300 8300
Wire Wire Line
	9500 8600 9500 8300
Wire Wire Line
	9500 8000 9500 7900
Wire Wire Line
	9300 7900 9500 7900
Connection ~ 9750 7900
Wire Wire Line
	9300 8000 9300 7900
Connection ~ 9500 7900
$Comp
L lumosity-rescue:Conn_01x01-conn J10
U 1 1 5A96DD4F
P 2700 4550
F 0 "J10" H 2700 4650 50  0000 C CNN
F 1 "Conn_01x01" H 2700 4450 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch1.27mm" H 2700 4550 50  0001 C CNN
F 3 "" H 2700 4550 50  0001 C CNN
	1    2700 4550
	-1   0    0    1   
$EndComp
$Comp
L lumosity-rescue:Conn_01x01-conn J9
U 1 1 5A96DE28
P 2500 4550
F 0 "J9" H 2500 4650 50  0000 C CNN
F 1 "Conn_01x01" H 2500 4450 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch1.27mm" H 2500 4550 50  0001 C CNN
F 3 "" H 2500 4550 50  0001 C CNN
	1    2500 4550
	1    0    0    1   
$EndComp
Wire Wire Line
	3050 4550 2900 4550
Wire Wire Line
	2150 4550 2300 4550
Wire Wire Line
	9000 4300 9000 4350
Wire Wire Line
	9350 5200 9350 5300
Wire Wire Line
	10150 5200 10250 5200
Wire Wire Line
	10700 5200 11000 5200
Wire Wire Line
	8100 2400 8100 2500
Wire Wire Line
	13300 2400 13300 2600
Wire Wire Line
	13300 2600 13300 3000
Wire Wire Line
	13300 3000 13300 3250
Wire Wire Line
	13300 2200 13300 2400
Wire Wire Line
	8100 2800 8200 2800
Wire Wire Line
	11000 4400 11150 4400
Wire Wire Line
	4300 3750 4300 3850
Wire Wire Line
	4300 3350 4300 3750
Wire Wire Line
	4300 3850 4300 4250
Wire Wire Line
	4500 3550 4500 3650
Wire Wire Line
	4500 3650 4500 3950
Wire Wire Line
	4500 3950 4500 4050
Wire Wire Line
	4500 4050 4500 4150
Wire Wire Line
	4500 4150 4500 4400
Wire Wire Line
	3250 3850 3250 4250
Wire Wire Line
	3250 3750 3250 3850
Wire Wire Line
	3250 3350 3250 3750
Wire Wire Line
	3050 3550 3050 3650
Wire Wire Line
	3050 3650 3050 3950
Wire Wire Line
	3050 3950 3050 4050
Wire Wire Line
	3050 4050 3050 4150
Wire Wire Line
	3050 4150 3050 4550
Wire Wire Line
	9200 2800 9200 2400
Wire Wire Line
	8000 9550 8100 9550
Wire Wire Line
	8000 9550 8000 9650
Wire Wire Line
	8000 7950 8050 7950
Wire Wire Line
	2850 7850 2950 7850
Wire Wire Line
	1850 8650 2100 8650
Wire Wire Line
	1850 9050 2150 9050
Wire Wire Line
	1150 9050 1150 9250
Wire Wire Line
	3850 1100 3850 1250
Wire Wire Line
	3850 1100 4000 1100
Wire Wire Line
	5000 1100 5150 1100
Wire Wire Line
	4400 1850 4400 1950
Wire Wire Line
	4400 1850 5150 1850
Wire Wire Line
	8700 8950 8550 8950
Wire Wire Line
	2850 10050 2850 10150
Wire Wire Line
	8900 3250 9200 3250
Wire Wire Line
	9750 7900 9750 8950
Wire Wire Line
	9500 7900 9750 7900
$EndSCHEMATC
