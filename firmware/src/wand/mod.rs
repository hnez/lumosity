extern crate stm32f0x0;

pub mod leds;
mod clock;
mod serial;
mod i2c;
pub mod accelerometer;

#[allow(unused)]
pub struct WandSensing {
    acc: Option<accelerometer::AccActive>,
    i2c: Option<i2c::I2CIdle>,
    clock: clock::ClockActive,
    leds: leds::LedsActive,
    serial: serial::SerialActive,
}

#[allow(unused)]
pub struct WandHibernating {
    acc: accelerometer::AccHibernating,
    i2c: i2c::I2CHibernating,
    clock: clock::ClockHibernating,
    leds: leds::LedsHibernating,
    serial: serial::SerialHibernating,
}

pub enum Wand {
    None,
    Hibernating(WandHibernating),
    Sensing(WandSensing),
}

impl WandSensing {
    pub fn new(rcc: stm32f0x0::RCC, gpioa: stm32f0x0::GPIOA, gpiob: stm32f0x0::GPIOB,
               tim3: stm32f0x0::TIM3, tim14: stm32f0x0::TIM14, syscfg: stm32f0x0::SYSCFG,
               scb: stm32f0x0::SCB, dma1: stm32f0x0::DMA1, usart1: stm32f0x0::USART1,
               i2c1: stm32f0x0::I2C1, exti: &stm32f0x0::EXTI, mut nvic: stm32f0x0::NVIC,
               pwr: stm32f0x0::PWR, acc_buffer: &'static mut [u8]) -> WandSensing {

        let clock = clock::ClockActive::new(&rcc, &syscfg, &scb, &pwr);
        let leds = leds::LedsActive::new(&rcc, &gpioa, &gpiob, tim3, tim14);
        let serial = serial::SerialActive::new(&rcc, &gpioa, &dma1, usart1);
        let i2c = i2c::I2CIdle::new(&rcc, &gpioa, &dma1, i2c1, &mut nvic);

        let (acc, i2c) = accelerometer::AccActive::new(exti, &mut nvic, i2c, acc_buffer);

        WandSensing {
            acc: Some(acc),
            i2c: Some(i2c),
            clock: clock,
            leds: leds,
            serial: serial,
        }
    }

    pub fn leds(&mut self) -> &mut leds::LedsActive {
        &mut self.leds
    }

    pub fn serial(&mut self) -> &mut serial::SerialActive {
        &mut self.serial
    }

    pub fn acc(&mut self) -> Option<accelerometer::AccActive> {
        self.acc.take()
    }

    pub fn acc_return(&mut self, acc: accelerometer::AccActive) {
        self.acc = Some(acc);
    }

    pub fn i2c(&mut self) -> Option<i2c::I2CIdle> {
        self.i2c.take()
    }

    pub fn i2c_return(&mut self, i2c: i2c::I2CIdle) {
        self.i2c = Some(i2c);
    }

    pub fn hibernate(mut self) -> WandHibernating {
        let leds = self.leds.hibernate();
        let serial = self.serial.hibernate();

        let i2c = self.i2c.take().unwrap();
        let (i2c, acc) = self.acc.take().unwrap().hibernate(i2c);
        let i2c = i2c.hibernate();

        let clock = self.clock.hibernate();

        WandHibernating {
            acc: acc,
            i2c: i2c,
            clock: clock,
            leds: leds,
            serial: serial,
        }
    }
}

impl WandHibernating {
    pub fn reset(&self) -> ! {
        self.clock.reset()
    }
}
