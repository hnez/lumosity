extern crate stm32f0x0;

const SLEEPDEEP: u32 = 0x00000004;
const SYSRESETREQ: u32 = 0x00000004;

use core;

pub struct ClockActive {
    rcc: *const stm32f0x0::RCC,
    syscfg: *const stm32f0x0::SYSCFG,
    scb: *const stm32f0x0::SCB,
    pwr: *const stm32f0x0::PWR,
}

pub struct ClockHibernating {
    rcc: *const stm32f0x0::RCC,
    syscfg: *const stm32f0x0::SYSCFG,
    scb: *const stm32f0x0::SCB,
    pwr: *const stm32f0x0::PWR,
}

#[allow(unsafe_code)]
unsafe impl core::marker::Send for ClockActive { }

#[allow(unsafe_code)]
unsafe impl core::marker::Send for ClockHibernating { }

impl ClockActive {
    #[allow(unsafe_code)]
    pub fn new(rcc: &stm32f0x0::RCC, syscfg: &stm32f0x0::SYSCFG,
               scb: &stm32f0x0::SCB, pwr: &stm32f0x0::PWR) -> ClockActive {
        /* - Setup HSI/2 as PLL input
         * - PLL multiply by 12
         * SYSCLK= 8MHz / 2 * 12 = 48MHz */
        unsafe {
            rcc.cfgr.modify(|_, w| w.pllsrc().bits(0)
                            .ppre().bits(0b000)
                            .pllmul().bits(0b1010));
        }

        // Turn on PLL
        rcc.cr.modify(|_, w| w.pllon().set_bit());

        // Setup PLL as main clock source
        unsafe {
            rcc.cfgr.modify(|_, w| w.sw().bits(0b10));
        }


        // Busy-loop until the PLL is setup
        while rcc.cfgr.read().sws().bits() != 0b10 {
        }

        // Enable gpio A/B clock
        rcc.ahbenr.modify(|_, w| w.iopaen().set_bit().iopben().set_bit());

        // Enable SYSCFG (WTF?!)
        rcc.apb2enr.modify(|_, w| w.syscfgen().set_bit());

        // Remap DMA USART DMA requests
        syscfg.cfgr1.modify(|_, w| w.usart1_tx_dma_rmp().set_bit()
                            .usart1_rx_dma_rmp().set_bit());

        // Enable DMA clock
        rcc.ahbenr.modify(|_, w| w.dma1en().set_bit());

        ClockActive {
            rcc: &(*rcc) as _,
            syscfg: &(*syscfg) as _,
            scb: &(*scb) as _,
            pwr: &(*pwr) as _,
        }
    }

    #[allow(unsafe_code)]
    pub fn hibernate(self) -> ClockHibernating {
        let pwr = unsafe { &(*self.pwr) };
        let scb = unsafe { &(*self.scb) };

        pwr.cr.write(|w| w.pdds().clear_bit());
        pwr.cr.write(|w| w.lpds().clear_bit());
        pwr.cr.write(|w| w.cwuf().set_bit());
        pwr.cr.write(|w| w.pdds().set_bit());
        pwr.cr.write(|w| w.lpds().set_bit());

        pwr.csr.write(|w| w.ewup1().set_bit());


        unsafe {
            scb.scr.write(SLEEPDEEP);
        }


        ClockHibernating {
            rcc: self.rcc,
            syscfg: self.syscfg,
            scb: self.scb,
            pwr: self.pwr,
        }
    }
}

impl ClockHibernating {
    #[allow(unsafe_code)]
    pub fn reset(&self) -> ! {
        let scb = unsafe { &(*self.scb) };

        unsafe {
            scb.aircr.write(SYSRESETREQ);
        }

        loop {
        }
    }
}
