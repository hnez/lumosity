pub struct ColorHSV {
    pub h: u16,
    pub s: u16,
    pub v: u16,
}

pub struct ColorRGB {
    pub r: u16,
    pub g: u16,
    pub b: u16,
}

impl ColorRGB {
    pub fn black() -> ColorRGB {
        ColorRGB::grey(0)
    }

    pub fn grey(v: u16) -> ColorRGB {
        ColorRGB {
            r: v, g: v, b: v,
        }
    }

    pub fn scale(&self, factor: u16) -> ColorRGB {
        ColorRGB {
            r: (((self.r as u32) * (factor as u32)) >> 16) as u16,
            g: (((self.g as u32) * (factor as u32)) >> 16) as u16,
            b: (((self.b as u32) * (factor as u32)) >> 16) as u16,
        }
    }

    pub fn mix_max(&self, other: ColorRGB) -> ColorRGB {
        ColorRGB {
            r: self.r.max(other.r),
            g: self.g.max(other.g),
            b: self.b.max(other.b),
        }
    }
}

impl ColorHSV {
    #[allow(unused)]
    pub const fn black() -> ColorHSV {
        ColorHSV {
            h: 0,
            s: 0,
            v: 0,
        }
    }

    #[allow(unused)]
    pub const fn white() -> ColorHSV {
        ColorHSV {
            h: 0,
            s: 0,
            v: 65535,
        }
    }

    #[allow(unused)]
    pub fn to_rgb(&self) -> ColorRGB {
        if self.s == 0 {
            return ColorRGB::grey(self.v);
        }

        let h = self.h as u32;
        let s = self.s as u32;
        let v = self.v as u32;

        let max = u16::max_value() as u32;
        let region_sz = max / 6;

        let region = h / region_sz;
        let pos = (h - region * region_sz) * 6;
        let npos = max - pos;

        let p = ((v * (max - s)) >> 16) as u16;
        let q = ((v * (max - ((s * pos) >> 16))) >> 16) as u16;
        let t = ((v * (max - ((s * npos) >> 16))) >> 16) as u16;

        let v = v as u16;

        match region {
            0 => ColorRGB{r: v, g: t, b: p},
            1 => ColorRGB{r: q, g: v, b: p},
            2 => ColorRGB{r: p, g: v, b: t},
            3 => ColorRGB{r: p, g: q, b: v},
            4 => ColorRGB{r: t, g: p, b: v},
            _ => ColorRGB{r: v, g: p, b: q},
        }
    }
}
