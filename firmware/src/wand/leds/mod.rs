extern crate stm32f0x0;

use core;

pub mod hsvrgb;

/* Using raw pointers here is really crapy but
 * but i could not figure out how to do it properly.
 * You won this time borrow checker */
pub struct LedsActive {
    tim3: stm32f0x0::TIM3,
    tim14: stm32f0x0::TIM14,

    rcc: *const stm32f0x0::RCC,
    gpioa: *const stm32f0x0::GPIOA,
    gpiob: *const stm32f0x0::GPIOB,
}

pub struct LedsHibernating {
    tim3: stm32f0x0::TIM3,
    tim14: stm32f0x0::TIM14,

    rcc: *const stm32f0x0::RCC,
    gpioa: *const stm32f0x0::GPIOA,
    gpiob: *const stm32f0x0::GPIOB,
}

#[allow(unsafe_code)]
unsafe impl core::marker::Send for LedsActive { }

#[allow(unsafe_code)]
unsafe impl core::marker::Send for LedsHibernating { }

impl LedsActive {
    #[allow(unsafe_code)]
    pub fn new(rcc: &stm32f0x0::RCC, gpioa: &stm32f0x0::GPIOA, gpiob: &stm32f0x0::GPIOB,
               tim3: stm32f0x0::TIM3, tim14: stm32f0x0::TIM14) -> LedsActive {
        // Reset timers and enable clocks
        rcc.apb1rstr.modify(|_, w| w.tim3rst().set_bit().tim14rst().set_bit());
        rcc.apb1rstr.modify(|_, w| w.tim3rst().clear_bit().tim14rst().clear_bit());
        rcc.apb1enr.modify(|_, w| w.tim3en().set_bit().tim14en().set_bit());

        /* Setup prescalers and counter tops
         * 48MHz / 2**16 = 732Hz */
        unsafe {
            tim3.psc.write(|w| w.psc().bits(0));
            tim3.arr.write(|w| w.arr_l().bits(0xffff));
            tim14.psc.write(|w| w.psc().bits(0));
            tim14.arr.write(|w| w.arr().bits(0xffff));
        }

        // Enable output compare
        unsafe {
            tim3.ccmr1_output.modify(|_, w| w.oc1m().bits(0b110)
                                     .oc1pe().set_bit()
                                     .oc2m().bits(0b110)
                                     .oc2pe().set_bit());

            tim3.ccmr2_output.modify(|_, w| w.oc4m().bits(0b110)
                                     .oc4pe().set_bit());

            tim14.ccmr1_output.modify(|_, w| w.oc1m().bits(0b110)
                                      .oc1pe().set_bit());
        }

        // Enable compare units
        tim3.ccer.modify(|_, w| w.cc1e().set_bit()
                         .cc2e().set_bit()
                         .cc4e().set_bit());

        tim14.ccer.modify(|_, w| w.cc1e().set_bit());

        // Enable outputs
        unsafe {
            gpioa.moder.modify(|_, w| w.moder4().bits(0b10)
                               .moder6().bits(0b10)
                               .moder7().bits(0b10));

            gpioa.afrl.modify(|_, w| w.afrl4().bits(0b0100)
                              .afrl6().bits(0b0001)
                              .afrl7().bits(0b0001));

            gpiob.moder.modify(|_, w| w.moder1().bits(0b10));
            gpiob.afrl.modify(|_, w| w.afrl1().bits(0b0001));
        }

        // Start timers
        tim14.cr1.modify(|_, w| w.cen().set_bit());
        tim3.cr1.modify(|_, w| w.cen().set_bit());

        let leds = LedsActive {
            tim3: tim3,
            tim14: tim14,

            rcc: rcc as *const _,
            gpioa: gpioa as *const _,
            gpiob: gpiob as *const _,
        };

        leds.set_white(0);

        leds
    }

    #[allow(unsafe_code)]
    fn set_pwm(&self, r: u16, g: u16, b: u16, wt: u16) {

        unsafe {
            self.tim3.ccr1.write(|w| w.ccr1_l().bits(r));
            self.tim3.ccr2.write(|w| w.ccr2_l().bits(g));
            self.tim3.ccr4.write(|w| w.ccr4_l().bits(b));

            self.tim14.ccr1.write(|w| w.ccr1().bits(wt));
        }
    }

    pub fn set_white(&self, w: u16) {
        self.set_pwm(0, 0, 0, w);
    }

    pub fn set_color(&self, color: hsvrgb::ColorRGB) {
        self.set_pwm(color.r, color.g, color.b, 0);
    }

    #[allow(unsafe_code)]
    pub fn hibernate(self) -> LedsHibernating {
        let (rcc, gpioa, gpiob) = unsafe {
            (&(*self.rcc), &(*self.gpioa), &(*self.gpiob))
        };

        rcc.apb1rstr.modify(|_, w| w.tim3rst().set_bit().tim14rst().set_bit());
        rcc.apb1enr.modify(|_, w| w.tim3en().clear_bit().tim14en().clear_bit());

        unsafe {
            gpioa.moder.modify(|_, w| w.moder4().bits(0b01)
                                    .moder6().bits(0b01)
                                    .moder7().bits(0b01));


            gpiob.moder.modify(|_, w| w.moder1().bits(0b01));
        }

        gpioa.odr.modify(|_, w| w.odr4().clear_bit()
                         .odr6().clear_bit()
                         .odr7().clear_bit());

        gpiob.odr.modify(|_, w| w.odr1().clear_bit());

        LedsHibernating {
            tim3: self.tim3,
            tim14: self.tim14,

            gpioa: self.gpioa,
            gpiob: self.gpiob,
            rcc: self.rcc,
        }
    }
}
