extern crate stm32f0x0;

use core;

use super::i2c;

const I2C_ADDR : u8 = 0x1d;

pub struct AccActive {
    exti: *const stm32f0x0::EXTI,
    buff: Option<&'static mut [u8]>,
}

enum ReadState {
    Addressing(i2c::I2CWriting),
    Reading(i2c::I2CReading),
}

pub enum PollResult {
    Done(i2c::I2CIdle, AccActive, Acceleration),
    Again(AccReading),
}

pub struct AccReading {
    state: ReadState,
    parent: AccActive,
}

pub struct AccHibernating {
    exti: *const stm32f0x0::EXTI,
    buff: &'static mut [u8],
}

pub struct Acceleration {
    pub x: i16,
    pub y: i16,
    pub z: i16,
}

#[allow(unsafe_code)]
unsafe impl core::marker::Send for AccActive { }

#[allow(unsafe_code)]
unsafe impl core::marker::Send for AccHibernating { }

impl AccActive {
    #[allow(unsafe_code)]
    pub fn new(exti: &stm32f0x0::EXTI, nvic: &mut stm32f0x0::NVIC,
               i2c: i2c::I2CIdle, buff: &'static mut [u8]) -> (AccActive, i2c::I2CIdle) {

        // Send to standby
        let i2c = i2c.write(I2C_ADDR, &[0x2d, 0]).wait();

        // Normal mode & 50Hz, Enable measurement, Enable data ready interrupt
        // Full resolution, justify left, +/-4g mode
        let i2c = i2c.write(I2C_ADDR, &[0x2C, 0b1001, 1<<3, 1<<7]).wait();
        let i2c = i2c.write(I2C_ADDR, &[0x31, 0x0d]).wait();

        // Setup interrupt mask and rising edge mask for the interrupt line
        exti.imr.modify(|_, w| w.mr0().set_bit());
        exti.rtsr.modify(|_, w| w.tr0().set_bit());

        // Enable interrupts and set priority
        unsafe {
            nvic.set_priority(stm32f0x0::Interrupt::EXTI0_1, 0);
        }
        nvic.enable(stm32f0x0::Interrupt::EXTI0_1);

        // Trigger interrupt
        exti.swier.modify(|_, w| w.swier0().set_bit());

        let acc = AccActive {
            exti: exti as *const _,
            buff: Some(buff),
        };

        (acc, i2c)
    }

    #[allow(unsafe_code)]
    pub fn read(self, i2c: i2c::I2CIdle) -> AccReading {
        let exti = unsafe { &(*self.exti) };
        exti.pr.write(| w | w.pr0().set_bit());

        let write = i2c.write(I2C_ADDR, &[0x32]);

        AccReading {
            state: ReadState::Addressing(write),
            parent: self,
        }
    }

    pub fn hibernate(mut self, i2c: i2c::I2CIdle) -> (i2c::I2CIdle, AccHibernating) {
        // Send to standby
        let i2c = i2c.write(I2C_ADDR, &[0x2d, 0]).wait();

        // Set Activity threshold to 0.625g, skip thres_inact, skip time_inact
        // AC Activity detection, Enable Axis X & Y & Z
        let i2c = i2c.write(I2C_ADDR, &[0x24, 10, 0, 0, 0xf0]).wait();

        // Enable measure & sleep mode, 2Hz wakeup rate
        // enable activity interrupt, route to int1
        let i2c = i2c.write(I2C_ADDR, &[0x2C, 0x17, 0x0e, 0x10, 0x00]).wait();

        let acc = AccHibernating {
            exti: self.exti,
            buff: self.buff.take().unwrap(),
        };

        (i2c, acc)
    }
}

impl AccReading {
    pub fn poll(mut self) -> PollResult {
        match self.state {
            ReadState::Addressing(write) => {
                let i2c = write.wait();
                let dst = self.parent.buff.take().unwrap();
                let read = i2c.read(I2C_ADDR, dst);

                let next = AccReading {
                    state: ReadState::Reading(read),
                    parent: self.parent,
                };

                PollResult::Again(next)
            },
            ReadState::Reading(read) => {
                let (i2c, dst) = read.wait();

                let acc = Acceleration {
                    x: (((dst[0] as u16) | (dst[1] as u16) << 8) as i16) / 8,
                    y: (((dst[2] as u16) | (dst[3] as u16) << 8) as i16) / 8,
                    z: (((dst[4] as u16) | (dst[5] as u16) << 8) as i16) / 8,
                };

                let parent = AccActive {
                    exti: self.parent.exti,
                    buff: Some(dst),
                };

                PollResult::Done(i2c, parent, acc)
            },
        }
    }

    pub fn wait(self) -> (i2c::I2CIdle, AccActive, Acceleration) {
        let mut read = Some(self);

        loop {
            match read.take().unwrap().poll() {
                PollResult::Again(next) => {
                    read = Some(next);
                },
                PollResult::Done(i2c, acc, acceleration) => {
                    return (i2c, acc, acceleration);
                }
            }
        }
    }
}
