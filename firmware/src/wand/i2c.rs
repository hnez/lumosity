extern crate stm32f0x0;

use core;

pub struct I2CIdle {
    i2c1: stm32f0x0::I2C1,

    dma1: *const stm32f0x0::DMA1,
    rcc: *const stm32f0x0::RCC,
    gpioa: *const stm32f0x0::GPIOA,
}

pub struct I2CWriting {
    parent: I2CIdle,
    src: &'static [u8],
}

pub struct I2CReading {
    parent: I2CIdle,
    dst: &'static mut [u8],
}

pub struct I2CHibernating {
    i2c1: stm32f0x0::I2C1,

    dma1: *const stm32f0x0::DMA1,
    rcc: *const stm32f0x0::RCC,
    gpioa: *const stm32f0x0::GPIOA,
}

#[allow(unsafe_code)]
unsafe impl core::marker::Send for I2CIdle { }

#[allow(unsafe_code)]
unsafe impl core::marker::Send for I2CHibernating { }

impl I2CIdle {
    #[allow(unsafe_code)]
    pub fn new(rcc: &stm32f0x0::RCC, gpioa: &stm32f0x0::GPIOA,
               dma1: &stm32f0x0::DMA1, i2c1: stm32f0x0::I2C1,
               nvic: &mut stm32f0x0::NVIC) -> I2CIdle {

        // Setup clock source to sysclk and reset the device
        rcc.cfgr3.modify(|_, w| w.i2c1sw().set_bit());
        rcc.apb1rstr.modify(|_, w| w.i2c1rst().set_bit());
        rcc.apb1rstr.modify(|_, w| w.i2c1rst().clear_bit());
        rcc.apb1enr.modify(|_, w| w.i2c1en().set_bit());

        // Setup SDA & SCL for AF1
        unsafe {
            gpioa.moder.modify(|_, w| w.moder9().bits(0b10).moder10().bits(0b10));
            gpioa.afrh.modify(|_, w| w.afrh9().bits(0b0100).afrh10().bits(0b0100));
        }

        // Setup timing constraints
        i2c1.timingr.write(|w| unsafe {
            w.presc().bits(4)
                .scll().bits(60)
                .sclh().bits(60)
                .sdadel().bits(12)
                .scldel().bits(12)
        });

        // Enable peripherial
        i2c1.cr1.write(|w| w.pe().set_bit()
                       .txdmaen().set_bit()
                       .rxdmaen().set_bit());

        // Enable interrupts and set priority
        unsafe {
            nvic.set_priority(stm32f0x0::Interrupt::DMA1_CH2_3, 1);
        }
        nvic.enable(stm32f0x0::Interrupt::DMA1_CH2_3);

        I2CIdle {
            i2c1: i2c1,

            dma1: &(*dma1) as *const _,
            rcc: &(*rcc) as *const _,
            gpioa: &(*gpioa) as *const _
        }
    }

    #[allow(unsafe_code)]
    pub fn write(self, addr: u8, src: &'static [u8]) -> I2CWriting {
        let dma1 = unsafe { &(*self.dma1) };

        let src_len = src.len();
        let src_addr = src.as_ptr() as u32;
        let txdr_addr = &self.i2c1.txdr as *const _  as u32;

        // Disable Channel to be able to change the registers
        dma1.ccr2.write(|w| w.en().clear_bit());

        unsafe {
            dma1.cpar2.write(|w| w.pa().bits(txdr_addr));
            dma1.cmar2.write(|w| w.ma().bits(src_addr));
            dma1.cndtr2.write(|w| w.ndt().bits(src_len as u16));
        }

        unsafe {
            dma1.ccr2.write(|w| w.msize().bits(0b00)
                            .psize().bits(0b00)
                            .minc().set_bit()
                            .pinc().clear_bit()
                            .dir().set_bit()
                            .tcie().set_bit()
                            .en().set_bit());
        }

        // Send start and address the device in write mode
        unsafe {
            self.i2c1.cr2.write(|w| w.sadd1().bits(addr)
                                .rd_wrn().clear_bit()
                                .nbytes().bits(src_len as u8)
                                .start().set_bit()
                                .autoend().set_bit()
            );
        }

        I2CWriting {
            parent: self,
            src: src,
        }
    }

    #[allow(unsafe_code)]
    pub fn read(self, addr: u8, dst: &'static mut [u8]) -> I2CReading {
        let dma1 = unsafe { &(*self.dma1) };

        let dst_len = dst.len();
        let dst_addr = dst.as_ptr() as u32;
        let rxdr_addr = &self.i2c1.rxdr as *const _  as u32;

        // Disable Channel to be able to change the registers
        dma1.ccr3.write(|w| w.en().clear_bit());

        unsafe {
            dma1.cpar3.write(|w| w.pa().bits(rxdr_addr));
            dma1.cmar3.write(|w| w.ma().bits(dst_addr));
            dma1.cndtr3.write(|w| w.ndt().bits(dst_len as u16));
        }

        unsafe {
            dma1.ccr3.write(|w| w.msize().bits(0b00)
                            .psize().bits(0b00)
                            .minc().set_bit()
                            .pinc().clear_bit()
                            .dir().clear_bit()
                            .tcie().set_bit()
                            .en().set_bit());
        }

        // Send start and address the device in write mode
        unsafe {
            self.i2c1.cr2.write(|w| w.sadd1().bits(addr)
                                .rd_wrn().set_bit()
                                .nbytes().bits(dst_len as u8)
                                .start().set_bit()
                                .autoend().set_bit()
            );
        }

        I2CReading {
            parent: self,
            dst: dst,
        }
    }

    pub fn hibernate(self) -> I2CHibernating {
        I2CHibernating {
            i2c1: self.i2c1,

            dma1: self.dma1,
            rcc: self.rcc,
            gpioa: self.gpioa,
        }
    }

}

impl I2CWriting {
    #[allow(unsafe_code)]
    pub fn wait(self) -> I2CIdle {
        let dma1 = unsafe { &(*self.parent.dma1) };

        while dma1.isr.read().tcif2().bit_is_clear() {
        }

        dma1.ifcr.write(|w| w.cgif2().set_bit());

        self.parent
    }
}

impl I2CReading {
    #[allow(unsafe_code)]
    pub fn wait(self) -> (I2CIdle, &'static mut [u8]) {
        let dma1 = unsafe { &(*self.parent.dma1) };

        while dma1.isr.read().tcif3().bit_is_clear() {
        }

        dma1.ifcr.write(|w| w.cgif3().set_bit());

        (self.parent, self.dst)
    }
}
