extern crate stm32f0x0;

use core;

pub struct SerialActive {
    usart1: stm32f0x0::USART1,

    dma1: *const stm32f0x0::DMA1,
    rcc: *const stm32f0x0::RCC,
    gpioa: *const stm32f0x0::GPIOA,
}

pub struct SerialHibernating {
    usart1: stm32f0x0::USART1,

    dma1: *const stm32f0x0::DMA1,
    rcc: *const stm32f0x0::RCC,
    gpioa: *const stm32f0x0::GPIOA,
}

#[allow(unsafe_code)]
unsafe impl core::marker::Send for SerialActive { }

#[allow(unsafe_code)]
unsafe impl core::marker::Send for SerialHibernating { }

impl SerialActive {
    #[allow(unsafe_code)]
    pub fn new(rcc: &stm32f0x0::RCC, gpioa: &stm32f0x0::GPIOA,
               dma1: &stm32f0x0::DMA1, usart1: stm32f0x0::USART1) -> SerialActive {
        unsafe {
            rcc.cfgr3.modify(|_, w| w.usart1sw().bits(0b01));
        }

        rcc.apb2rstr.modify(|_, w| w.usart1rst().set_bit());
        rcc.apb2rstr.modify(|_, w| w.usart1rst().clear_bit());
        rcc.apb2enr.modify(|_, w| w.usart1en().set_bit());

        // Setup RX & TX for AF1
        unsafe {
            gpioa.moder.modify(|_, w| w.moder2().bits(0b10).moder3().bits(0b10));
            gpioa.afrl.modify(|_, w| w.afrl2().bits(0b0001).afrl3().bits(0b0001));
        }


        let brr = 48_000_000 / 115_200;
        unsafe {
            usart1.brr.write(|w| w.bits(brr));
        }

        // Enable DMA for Transmit
        usart1.cr3.modify(|_, w| w.dmat().set_bit());

        // Enable UART receive / transmit
        usart1.cr1.write(|w| w.ue().set_bit().re().set_bit().te().set_bit());

        SerialActive {
            usart1: usart1,

            dma1: dma1 as *const _,
            rcc: rcc as *const _,
            gpioa: gpioa as *const _,
        }
    }
}

impl SerialActive {
    #[allow(unsafe_code)]
    pub fn write_slice(&mut self, s: &'static [u8]) {
        let src_addr = s.as_ptr() as u32;
        let dst_addr = &self.usart1.tdr as *const _  as u32;
        let length = s.len() as u16;

        // Wait for previous transfer to end
        self.flush();

        let dma1 = unsafe { &(*self.dma1) };

        // Disable Channel to be able to change the registers
        dma1.ccr4.write(|w| w.en().clear_bit());

        unsafe {
            dma1.cpar4.write(|w| w.pa().bits(dst_addr));
            dma1.cmar4.write(|w| w.ma().bits(src_addr));
            dma1.cndtr4.write(|w| w.ndt().bits(length));
        }

        unsafe {
            dma1.ccr4.write(|w| w.msize().bits(0b00)
                            .psize().bits(0b00)
                            .minc().set_bit()
                            .pinc().clear_bit()
                            .dir().set_bit()
                            .en().set_bit());
        }
    }

    #[allow(unsafe_code)]
    pub fn flush(&self) {
        let dma1 = unsafe { &(*self.dma1) };

        while dma1.cndtr4.read().ndt().bits() != 0 {
        }
    }

    pub fn hibernate(self) -> SerialHibernating {
        SerialHibernating {
            usart1: self.usart1,

            dma1: self.dma1,
            rcc: self.rcc,
            gpioa: self.gpioa,
        }
    }
}
