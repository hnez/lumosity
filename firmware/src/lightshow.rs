use wand::leds::hsvrgb::{ColorRGB, ColorHSV};

static COLORS : [ColorHSV; 8] = [
    ColorHSV{h: 16274, s: 0xffff, v: 0xffff},
    ColorHSV{h: 62695, s: 0xffff, v: 0xffff},
    ColorHSV{h: 35516, s: 0xffff, v: 0xffff},
    ColorHSV{h: 5971, s: 0xffff, v: 0xffff},
    ColorHSV{h: 9065, s: 19201, v: 0xffff},
    ColorHSV{h: 46420, s: 0xffff, v: 0xffff},
    ColorHSV{h: 26159, s: 0xffff, v: 0xffff},
    ColorHSV{h: 34952, s: 0xffff, v: 0xffff},
];

pub struct Lightshow {
    intensities: [u16; 8],
}

impl Lightshow {
    pub fn new() -> Self {
        Lightshow {
            intensities: [0u16; 8],
        }
    }

    pub fn feed(&mut self, channels: [bool; 8]) -> ColorRGB {
        for (i, &c) in self.intensities.iter_mut().zip(channels.iter()) {
            *i = if c { 0xffff } else {(((*i) as u32) * 15 / 16) as u16};
        }

        let color = COLORS.iter().zip(self.intensities.iter())
            .map(|(c, &i)| c.to_rgb().scale(i))
            .fold(ColorRGB::black(), |c1, c2| c1.mix_max(c2));

        color
    }
}
