pub struct Activity {
    mean: [i16; 3],
    inact_time: u16,
}

impl Activity {
    pub fn new() -> Self {
        Activity {
            mean: [0; 3],
            inact_time: 0
        }
    }

    pub fn feed(&mut self, input: [i16; 3]) {
        self.mean[0] = (((self.mean[0] as i32) * 7 + (input[0] as i32)) / 8) as i16;
        self.mean[1] = (((self.mean[1] as i32) * 7 + (input[1] as i32)) / 8) as i16;
        self.mean[2] = (((self.mean[2] as i32) * 7 + (input[2] as i32)) / 8) as i16;

        let dx = (input[0] - self.mean[0]) as i32;
        let dy = (input[1] - self.mean[1]) as i32;
        let dz = (input[2] - self.mean[2]) as i32;

        let d = dx * dx + dy * dy + dz * dz;

        self.inact_time = if d < 20000 { self.inact_time.saturating_add(1) } else { 0 };
    }

    pub fn is_active(&self) -> bool {
        self.inact_time < 300
    }

    pub fn is_inactive(&self) -> bool {
        !self.is_active()
    }
}
