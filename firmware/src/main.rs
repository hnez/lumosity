#![deny(unsafe_code)]
#![feature(proc_macro)]
#![feature(const_fn)]
#![feature(exact_chunks)]
#![no_std]

extern crate cortex_m_rtfm as rtfm;
extern crate stm32f0x0;
extern crate heapless;

mod wand;
mod network;
mod lightshow;
mod activity;

use wand::accelerometer::Acceleration;
use heapless::{RingBuffer, consts, ring_buffer::{Producer, Consumer}};
use rtfm::{Resource, Threshold, app};

app! {
    device: stm32f0x0,

    resources: {
        static WAND: wand::Wand;

        static ACC_BUFFER: [u8; 6] = [0u8; 6];

        static ACC_READ: Option<wand::accelerometer::AccReading> = None;

        static ACC_QUEUE: RingBuffer<Acceleration, consts::U5, u8> = RingBuffer::u8();
        static ACC_QUEUE_PROD: Producer<'static, Acceleration, consts::U5, u8>;
        static ACC_QUEUE_CONS: Consumer<'static, Acceleration, consts::U5, u8>;

        static NET: network::Network = network::Network::new(&network::CONV1, &network::CONV2, &network::CONV3);
    },

    init: {
        resources: [ACC_QUEUE, ACC_BUFFER],
    },

    idle: {
        resources: [WAND, ACC_QUEUE_CONS, NET],
    },

    tasks: {
        EXTI0_1: {
            path: acc_rdy_irq,
            resources: [WAND, ACC_READ],
        },

        DMA1_CH2_3: {
            path: acc_dma_irq,
            resources: [WAND, ACC_READ, ACC_QUEUE_PROD],
        },
    },
}

fn init(p: init::Peripherals, r: init::Resources) -> init::LateResources {
    let rcc = p.device.RCC;
    let gpioa = p.device.GPIOA;
    let gpiob = p.device.GPIOB;
    let tim3 = p.device.TIM3;
    let tim14 = p.device.TIM14;
    let usart1 = p.device.USART1;
    let dma1 = p.device.DMA1;
    let syscfg = p.device.SYSCFG;
    let i2c1 = p.device.I2C1;
    let exti = p.device.EXTI;
    let pwr = p.device.PWR;

    let nvic = p.core.NVIC;
    let scb = p.core.SCB;

    let acc_queue: &'static mut _ = r.ACC_QUEUE;
    let acc_buffer: &'static mut _ = r.ACC_BUFFER;

    let wand = wand::WandSensing::new(rcc, gpioa, gpiob, tim3, tim14,
                                      syscfg, scb, dma1, usart1, i2c1, &exti,
                                      nvic, pwr, acc_buffer);

    let wand = wand::Wand::Sensing(wand);

    let (acc_queue_prod, acc_queue_cons) = acc_queue.split();

    init::LateResources {
        WAND: wand,
        ACC_QUEUE_PROD: acc_queue_prod,
        ACC_QUEUE_CONS: acc_queue_cons,
    }
}

#[allow(unsafe_code)]
fn idle(t: &mut rtfm::Threshold, mut r: idle::Resources) -> ! {
    let mut lightshow = lightshow::Lightshow::new();
    let mut active = activity::Activity::new();

    loop {
        // Phase 1: Greeting - Turn on white LEDs for 10s
        r.WAND.claim_mut(t, |wand, _t| {
            if let wand::Wand::Sensing(ref mut w) = wand {
                w.serial().write_slice(b"Hey!\r\n");
                w.leds().set_white(65000);
            }
        });

        for _i in 0..500 {
            while r.ACC_QUEUE_CONS.dequeue().is_none() {
                rtfm::wfi();
            }
        }

        // Phase 2: End Greeting - Fade out white LEDs for 2s
        for i in 0..100 {
            while r.ACC_QUEUE_CONS.dequeue().is_none() {
                rtfm::wfi();
            }

            r.WAND.claim_mut(t, |wand, _t| {
                if let wand::Wand::Sensing(ref mut w) = wand {
                    w.leds().set_white(65000 - i * 650);
                }
            });
        }

        // Phase 3: Witchcraft
        while active.is_active() {
            if let Some(acceleration) = r.ACC_QUEUE_CONS.dequeue() {
                let input = [acceleration.x, acceleration.y, acceleration.z];

                active.feed(input);

                if let Some(output) = r.NET.forward(input) {
                    let mut channels = [false; 8];

                    output.iter().map(|&v| v > 1000)
                        .zip(channels.iter_mut())
                        .for_each(|(s, c)| *c = s);

                    let color = lightshow.feed(channels);

                    r.WAND.claim_mut(t, |wand, _t| {
                        if let wand::Wand::Sensing(ref mut w) = wand {
                            w.leds().set_color(color);
                        }
                    });
                }
            }
        }

        r.WAND.claim_mut(t, |wand, _t| {
            if let wand::Wand::Sensing(ref mut w) = wand {
                w.leds().set_white(0);
            }
        });

        // Phase 4: Wait
        while active.is_inactive() {
            if let Some(acceleration) = r.ACC_QUEUE_CONS.dequeue() {
                let input = [acceleration.x, acceleration.y, acceleration.z];

                active.feed(input);
            }
        }
    }
}

fn acc_dma_irq(t: &mut Threshold, mut r: DMA1_CH2_3::Resources) {
    if let wand::Wand::Sensing(ref mut w) = r.WAND.borrow_mut(t) {
        let acc_read = r.ACC_READ.borrow_mut(t);
        let acc_queue_prod = r.ACC_QUEUE_PROD.borrow_mut(t);

        if acc_read.is_some() {
            match acc_read.take().unwrap().poll() {
                wand::accelerometer::PollResult::Again(next) => {
                    *acc_read = Some(next);
                },
                wand::accelerometer::PollResult::Done(i2c, acc, acceleration) => {
                    w.acc_return(acc);
                    w.i2c_return(i2c);

                    acc_queue_prod.enqueue(acceleration).ok().unwrap();
                }
            }
        }
    }
}

#[allow(unsafe_code)]
fn acc_rdy_irq(t: &mut Threshold, mut r: EXTI0_1::Resources) {
    let acc_read = r.ACC_READ.borrow_mut(t);

    if let wand::Wand::Sensing(ref mut w) = r.WAND.borrow_mut(t) {
        let acc = w.acc().unwrap();
        let i2c = w.i2c().unwrap();

        let reading = acc.read(i2c);
        *acc_read = Some(reading);
    }
}
