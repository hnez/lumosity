Lumosity
========

![PCB Demo](images/lumos_pcb.jpg)

A Gesture recognizing wand.

This project may be of interest to you mainly for the
tools and techniques used and for the dataset of
accelerometer readings of gestures.

Version 0.1 of the lumosity project consists of the
following parts:

- A PCB containing an ADXL345 accelerometer and an STM32F030
  microcontroller.
- A 1D Convolutional Neural Network model trained using PyTorch,
  taking accelerometer recordings as input and outputting
  gesture probabilities.
- A firmware written in Rust, using the cortex-m-rtfm library,
  that uses the trained neural network for gesture recognition.
